﻿#include <iostream>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
    const int N = 7;
    int array[N][N];
    int sum = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            cout << setw(7) << array[i][j];
        }
        cout << "\n";
    }
    cout << "\n";
    tm ptr;
    time_t lt = time(nullptr);
    localtime_s(&ptr, &lt);
    int DayString = ptr.tm_mday % 2;
    for (int j = 0; j < N; j++)
    {
        sum = sum + array[DayString][j];
    }
    cout << "Sum -  " << sum << "\n";
}